\chapter{Numerical simulation}
\label{chap: NumericalSimulation}

The numerical simulation so as his input parameters and results will be explained in detail in this chapter. The simulation was carried out using the software Plaxis 3\,D. The whole pile was modeled in the middle of the soil mass. The water level was modeled at the same level as the top of the sand soil (saturated soil). Because of the cohesionless soil, modelling the water level above the top of the soil would have no other effect than incrementing the calculation time of the model. Two hammer blows were modeled to obtain the tendency of the effect on the inclined pile. Due to the eccentric load on the MP, an horizontal load is produced and a spatial deformation state is generated, so the problem has to be modeled in 3\,D. The simulation unknown is the displacement field $\mathbf{u}(\mathbf{x},t)$ \parencite{Stanford2018}.

A dynamic deformation analysis was executed in order to simulate the effects of a completely vertical load (water hammer, see section \ref{subchap: WaterHammer}) on a lightly inclined pile during the driving process. The numerical simulation was performed using Plaxis 3\,D software and three different models were elaborated, with the only difference being the pile inclination. One model was run with a completely vertical pile, the second with a pile inclined 0,25\,$^\circ$ and the last model with a pile inclined 0,50\,$^\circ$. A concentric load was also simulated on the models with an inclined pile in order to compare the performances between the water hammer and an hydraulic hammer.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Input parameters}
\label{subchap: InputParameters}

The input parameters to simulate the different elements on the model will be presented and explained in detail. It is important to consider the options selected in the program to fully understand what exactly is being simulated.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection{Soil parameters}
\label{subchap: SoilParameters} 

The dimensions of the soil in the model were selected based on the pile which has a diameter of 6,5\,m, 56,6\,m length and is modeled as wished in place with a penetration of 13\,m. Considering that the selected dimension for the soil mass are:

\begin{itemize}
	\item x direction: 100\,m
	\item y direction: 100\,m
	\item z direction: 90\,m 
\end{itemize}

The soil displacement is fixed in all directions at the bottom as well as in the x and y directions at the sides. There is no load at the top of the soil.

 The soil, in frame of this project, was selected as pure sand and is modeled as one layer. It was simulated using continuous elements for the spacial deformation state with three displacement and two pore pressure degrees of freedom (DOF) per node. A higher discretization grade was selected in the areas where greater deformation is expected, in this case at the toe of the pile. The soil mesh is showed in Figure \ref{SoilMesh} where a higher element density is to be seen in the zone where the pile will be placed and a few meters below.   

\begin{figure}
	\begin{center}
		\includegraphics[scale=0.7]{Soil_mesh}
		\caption{Soil mesh.}
		\label{SoilMesh}
	\end{center}
\end{figure}

The model chosen to simulate the soil is the Hardening soil model (HSM). This is a non-linear elastic hardening plastic model with Mohr-Coulomb failure criterion. It utilizes three yield surfaces that includes deviatoric (shear), volumetric (cap) and tension cut off. The HSM is suited to describe the barotropic behavior, stiffness at small strains, failure criterion (Mohr-Coulomb) and dilatancy behavior of the soil \parencite{Stanford2018}. According to that, the HSM is adequate to simulate this problem. The model parameters used in the Plaxis simulation were taken from a sandy soil location of the north sea, whose laboratory results can be seen in Figures \ref{TriaxDeviatoricAndVolumetric} and \ref{TriaxResultsMohrCircle}. The laboratory test were reproduced using the SoilTest module from Plaxis and the parameters where optimized to best fit of the curves in Figures \ref{TriaxDeviatoricAndVolumetric} and \ref{TriaxResultsMohrCircle}. This allows to test whether the behavior of soil materials in Plaxis 3\,D matches the measurements and expectation. The resulted graphs from the reproduced test are presented in Figures \ref{DeviatoricAndVolumetricPlaxis} and \ref{MorhCirclePlaxis} . The numerical values of the soil input parameters are shown in the Table \ref{tab: HSMParameters}.

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.5]{Deviatoric_and_volumetric_strain}
		\caption{Laboratory results from the sand soil at the north sea deviatoric stress and volumetric strain.}
		\label{TriaxDeviatoricAndVolumetric}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.5]{Mohr_circle}
		\caption{Laboratory results from the sand soil at the north sea p-q diagram and Morh circle.}
		\label{TriaxResultsMohrCircle}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.82]{Deviatoric_and_volumetric_strain_plaxis}
		\caption{Plaxis simulation deviatoric stress and volumetric strain.}
		\label{DeviatoricAndVolumetricPlaxis}
	\end{center}
\end{figure}

\begin{figure}[!]
	\begin{center}
		\includegraphics[scale=0.82]{Mohr_circle_plaxis}
		\caption{Plaxis simulation p-q diagram and Mohr circle.}
		\label{MorhCirclePlaxis}
	\end{center}
\end{figure}

\begin{table}[ht!]
	\centering
	\caption{Hardening soil model parameters.}
	\begin{tabular}{ |c|c|c| }
		\hline
		Drainage type & Drained & [-] \\ \hline
		$\gamma_{unsat}$ & 18,60 & [$\frac{kN}{m^{3}}$] \\ \hline
		$\gamma_{sat}$ & 20,00 & [$\frac{kN}{m^{3}}$] \\ \hline
		$e_{init}$ & 0,674 & [-] \\ \hline
		$E_{50}^{ref}$ & 49590 & [$\frac{kN}{m^{2}}$] \\ \hline
		$E_{eod}^{ref}$ & 49590 & [$\frac{kN}{m^{2}}$] \\ \hline
		$E_{ur}^{ref}$ & 148800 & [$\frac{kN}{m^{2}}$] \\ \hline
		$m$     & 0,40  & [-] \\ \hline
		$c^{'}_{ref}$ & 18,00 & [$\frac{kN}{m^{2}}$] \\ \hline
		$\phi$ & 34,00 & [$^\circ$] \\ \hline
		$\psi$ & 7,14  & [$^\circ$] \\ \hline
		$\nu^{'}_{ur}$ &  0.20 & [-] \\ \hline
		$p_{ref}$ & 100 & [$\frac{kN}{m^{2}}$] \\ \hline
		$K_{0}^{nc}$ & 0,4408 & [-] \\ \hline
		$R_{f}$ & 0,90  & [-] \\ \hline
		OCR   & 1,00  & [-] \\ \hline
	\end{tabular}
	\label{tab: HSMParameters}
\end{table}

The parameters $\nu^{'}_{ur}$, $E_{eod}^{ref}$, $\sigma^{ref}$ and $R_{f}$ were taken from the literature recommendations for sand soils. For further information on parameter estimation for the HSM see \cite{ObrzudHSM} and \cite{brinkgreveHSM}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection{Pile simulation}
\label{subchap: PileSimulation} 

The pile used in the simulation has a length of 56,6\,m, diameter of 6,5\,m and a constant wall thickness from top to bottom of 75\,mm (see Table \ref{tab: PileCharacteristics}). It is activated as "wished in place" at a penetration depth of 13\,m. This kind of simulation is a simplification of the reality, because the pile will be activated at 13\,m of penetration depth at a stress-less state. The pile is modeled with membrane elements as surface with three translation DOF per node (rigid body) and a linear elastic steel material (see Table \ref{tab: SteelProperties}). The weight of the pile is applied as load at the reference point of the rigid body, which was set up at the center of mass of the pile.

\begin{table}[h!]
	\centering
	\caption{Pile characteristics.}
	\begin{tabular}{ |c|c|c| }
		\hline
		Length & 56,60 & [m] \\ \hline
		Pile diameter & 6,50 & [m] \\ \hline
		Wall thickness & 75    & [mm] \\ \hline
		Total mass & 6726,21 & [kN] \\ \hline
		Toe area & 1,51  & [$m^{2}$] \\ \hline
		Initial penetration & 13    & [m] \\ \hline
	\end{tabular}
	\label{tab: PileCharacteristics}
\end{table}

The pile - soil contact is simulated by interface elements inside and outside the pile, with a virtual thickness of 0,075\,m and the same steel material as the pile in order to account the toe bearing. The rigid body translation conditions were set to forces meanwhile the rotation conditions were set to moment.
 
The only difference between the three models mentioned above is the inclination of the pile 0\,$^\circ$, 0,25\,$^\circ$ and 0,50\,$^\circ$ inclination regarding the vertical direction z.     

\begin{table}[h!]
	\centering
	\caption{Pile steel properties.}
	\begin{tabular}{ |c|c|c| }
		\hline
		%$\gamma$ & 78.50 & [$\frac{kN}{m^{3}}$] \\ \hline
		E     & 210.000.000 & [$\frac{kN}{m^{2}}$] \\ \hline
		$\nu$ & 0,30 & [-] \\ \hline
		Material type & Non porous & [-] \\ \hline
	\end{tabular}%
	\label{tab: SteelProperties}%
\end{table}%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection{Load input}
\label{subchap: LoadInput} 

The dynamic load input is shown in Figure \ref{2BlowGraph}. This load is an estimation of two consecutive blows of a water hammer during a period of time of 12\,sec. The hammer itself is considered with a water level such that it weights 20\,MN. Figure \ref{2BlowGraph} shows clearly both impacts of the hammer per blow (see Section \ref{subchap: WaterHammer}) with a time difference of 0,70\,sec and a peak of 67,85\,MN. This load is transferred to the pile via a vertical line load at the top of the pile. For the purpose of comparison the same load was simulated eccentric and concentric on both inclined piles to compare the efficiency of a water hammer with an hydraulic hammer. The mentioned load inclination was carried out using the vector components in x and z direction for the concentric load and only in z direction for the eccentric load.         

\begin{figure}[!]
	\begin{center}
		\includegraphics[scale=0.7]{2_blows_graph}
		\caption{Force input signal.}
		\label{2BlowGraph}
	\end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Mesh generation}
\label{subchap: MeshGeneration} 

The coarseness factor for the mesh generation was set up to 0,25 at the entire pile as well as for a soil volume sector were the pile is placed (25x25x25\,m). The coarseness factor of the rest of the soil was set to 0,50. The mesh was generated with a "Fine" element distribution. It is of knowledge of the author that while using only one mesh the convergence of the numeric problem can not be studied, but due to a time limitation the finer possible mesh was selected to execute the calculations.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Calculation phases}
\label{subchap: CalculationPhases} 

The simulation was carried on using three different calculations phases with a tolerated error of 0,0100. All three models have the same following calculation phases:

\begin{enumerate}
	\item Initial phase
	\item Pile activation 
	\item Dynamic calculation
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection{Initial phase}
\label{subchap: InitialPhase} 

In this phase only the soil mass is activated and the $K_{0}$ state is determined (see Figure \ref{K0State}) with the water level set at the same level than the top soil mass coordinate. The selected calculation type for this phase is "K0 procedure".

\begin{figure}
	\begin{center}
		\includegraphics[scale=0.2]{K0_state}
		\caption{Initial phase $K_{0}$ state.}
		\label{K0State}
	\end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection{Pile activation phase}
\label{subchap: PileActivation} 

Here the pile including the interface elements is activated as "wished in place" at a penetration depth of 13\,m. The pile weight is the only load activated at this stage causing a minimal vertical displacement of the pile. The calculation type of this phase was set up to "Plastic" and it starts after the initial phase.  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection{Dynamic calculation phase}
\label{subchap: DynamicCalculation} 

The vertical line load (see Figure \ref{2BlowGraph}) is activated and the dynamic calculation over a total time of 12\,sec, were two hammer blows are simulated set up. Here the calculation type is set to "Dynamic" with a total of 100 steps and 5 sub steps. The maximal number of steps stored is set to 70 and the max number of iterations to 60. The dynamic phase starts after the pile activation phase and considers the deformation state of the that phase.

In the models with an inclined pile an extra dynamic phase is added with the same parameters and start point (see Figure \ref{CalculationPhases}), but the only difference is that the line is applied concentric to the pile. This is made in order to compare the efficiency of a water hammer and an hydraulic hammer (eccentric and concentric respectively).

\begin{figure}
	\begin{center}
		\includegraphics[scale=1]{Calculation_Phases}
		\caption{Calculation phases.}
		\label{CalculationPhases}
	\end{center}
\end{figure}  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Plausibility check}
\label{subchap: PlausibilityCheck}

To check the plausibility of the model results, a series of figures will be presenting containing the most relevant information to proof if the results are in frame of what is expected. All the Figures showed in this sections were taken from the model with a vertical pile.

The first check executed was to establish if the dimension of the model is appropriated. This is done by assuring that the border conditions does not have any influence on the stresses and/or displacements near the borders. To do so the principal effective stresses $\sigma^{'}_{2}$ and the total displacements $|u|$ are shown in Figures \ref{ToePlaneEffectiveStresses} and \ref{ToePlaneDisplacements} respectively. The principal effective stresses $\sigma^{'}_{2}$ are perpendicular to the borders, which means that the border conditions does not have any effect on the stresses in this directions. In a similar way the border conditions doesn't determine the total displacements which have a perfectly symmetric distribution as seen in Figure \ref{ToePlaneEffectiveStresses}. 

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.35]{Pile_Toe_Plane_Total_sigma2_Effective_Stresses}
		\caption{Principal effective stresses $\sigma^{'}_{2}$ principal directions.}
		\label{ToePlaneEffectiveStresses}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.35]{Pile_Toe_Plane_Total_Displacements}
		\caption{Total displacements $|u|$.}
		\label{ToePlaneDisplacements}%
	\end{center}
\end{figure}

Another control that was carried out is the capacity of the model to simulate the toe resistance. This is shown with the effective stresses $\sigma^{'}_{zz}$ on the horizontal plane at the pile toe height (Figure \ref{PileToePlaneSigmazz}). It is clearly seen that the pile vertical effective stresses at the pile toe are simulated as expected, with the highest stresses directly under the pile toe and a limited "circle of influence" around it with the border conditions having no effect on the stresses.

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.22]{Pile_Toe_Plane_Sigmazz}
		\caption{$\sigma^{'}_{zz}$ stresses on the horizontal plane at the pile toe height.}
		\label{PileToePlaneSigmazz}
	\end{center}
\end{figure} 

To check that the model can simulate the shaft resistance correctly, the stresses $\sigma^{'}_{zz}$ in a plane that cuts the pile in the middle are displayed in Figure \ref{PileShaftSigmazz}. A variation of the stresses inside and outside the pile is to be seen, meaning that the model is able to simulate the shaft resistance as expected.

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.22]{Horizontal_Plane_xz_vertical_stresses}
		\caption{$\sigma^{'}_{zz}$ stresses on the vertical plane z-x.}
		\label{PileShaftSigmazz}
	\end{center}
\end{figure} 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\section{Simulation results}
\label{subchap: Results} 

As explained above three different models were developed within this project:
\begin{itemize}
	\item[a.] The pile completely vertical
	\item[b.] The pile with 0,25\,$^\circ$ inclination
	\item[c.] The pile with 0,50\,$^\circ$ inclination
\end{itemize}

The inclination angle $\alpha$ is measured at plane z-x as shown in Figure \ref{PileSketch} \protect\subref{PileInclinationSketch} while at z-y the pile has no inclination. This means that the $u_{x}$ displacements will be of central meaning for the analysis, not being the case of the displacements in y direction. 

The models with an inclined pile have two load cases each. One case with a vertical load (eccentric load) to represent the water hammer named "ecc" and the other with the load in the same orientation as the pile axis (concentric load) representing the load of an hydraulic hammer abbreviated "conc". The results will be presented referring to the load cases as ecc and conc. All the data was taken from the models with the output module of Plaxis and then post processed using the software Matlab.

To report and analyze the results, the displacements from four points at the pile toe (-13\,m) and at the ground level (0\,m) were examined. The point designation is shown in Figure \ref{PilePoints}. The mass center displacements of the pile is also being analyzed.

\begin{figure}[t]
	\begin{center}
		\subfloat[Pile inclination in z-x plane.]{\label{PileInclinationSketch}%
			\includegraphics[scale=0.6]{Pile_sketch}}
		\hspace*{1cm}
		\subfloat[Pile points designation.]{\label{PilePoints}%
			\includegraphics[scale=0.5]{Pile_points}}
		\caption[]{Pile inclination and point designations.}
		\label{PileSketch}
	\end{center}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\subsection{Driving efficiency}
\label{subchap: HammerEfficiency} 

To analyze the efficiency to drive the pile of both hammers the $u_{z}$ displacement data at the pile toe, means the penetration depth of the pile, was used. The mean displacement at the pile toe in the vertical direction z after one and two blows is shown in Table \ref{tab:UzMeanDisplacement}. Figures \ref{UzMassCenterDisplacements} and \ref{UzToeDisplacements} shows a comparison between all the models vertical displacements $u_{z}$ for the center of mass points and the mean displacements of the four nodes analyzed at the pile toe. As expected both displacements are almost identical, they have a difference of 2\,\% each model between the vertical displacements at the mass center and at the pile toe. This may be due to the fact that not every node at the pile toe was taken into account nonetheless a 2\,\% difference is a good approach.
 
\begin{table}[h!]
 \centering
 \caption{$u_{z}$ mean displacement at the pile toe after one and two blows.}
 	\begin{tabular}{|c|c|c|}
 		\hline
 		Model & $u_{z}$ one after blow [m] & $u_{z}$ after two blows [m]  \\ \hline
 		Model 1 & 0,1408 & 0,1587   \\ \hline
 		Model 2 ecc & 0,1427 & 0,1642  \\ \hline
 		Model 2 conc & 0,1429 & 0,1628 \\ \hline
 		Model 3 ecc & 0,1460 & 0,1681  \\ \hline
 		Model 3 conc & 0,1460 & 0,1653  \\ \hline
 	\end{tabular}
 	\label{tab:UzMeanDisplacement}
 \end{table}
 
 \begin{figure}[H]
 	\begin{center}
 		\includegraphics[scale=0.8]{Mass_center_Uz_displacements}
 		\caption{Mass center $u_{z}$ displacements.}
 		\label{UzMassCenterDisplacements}%
 	\end{center}
\end{figure}

 \begin{figure}[H]
	\begin{center}
 		\includegraphics[scale=0.8]{Toe_Uz_displacements}
 		\caption{Pile toe mean $u_{z}$ displacements.}
 		\label{UzToeDisplacements}
 	\end{center}
 \end{figure}
 
 The driving efficiency was calculated as the difference in percentage of the $u_{z}$ mean toe displacements between the eccentric and concentric blow for the models with an inclined pile. In this simulation the driving efficiency between both hammer types studied did not showed significant differences. After the first blow the concentric strike is more efficient with a 0,14\,\% and 0,03\,\% for model with 0,25\,$^\circ$ and 0,50\,$^\circ$ pile inclination angle respectively. As for the second blow the eccentric strike shows to be more efficient with 0,88\,\% and 1,66\,\% for model with 0,25\,$^\circ$ and 0,50\,$^\circ$ pile inclination angle respectively. These small differences are not significant and do not show a tendency, that is why it can't be said based on the present investigation which hammer is more effective in terms of penetration per blow.

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %
 \subsection{Pile inclination effects}
 \label{subchap: PileInclinationEffects} 
 
 The graph in Figure \ref{MassCenterUxDisplacements} shows the $u_{x}$ pile mass center displacements over the time of the simulation. In this graph a huge difference between the eccentric and concentric load cases can be acknowledge. For the eccentric load cases in the model with 0,25\,$^\circ$ and 0,50\,$^\circ$ pile inclination angle the lateral displacements are much higher than in the other cases. This probably means that the inclination angle grew after the two eccentric hammer blows. For the case with no inclination and the ones with the pile driven concentrically the lateral $u_{x}$ displacements are close to zero. aaaa explicar despl de modelo 1
 
 \begin{figure}[!]
 	\begin{center}
 		\includegraphics[scale=0.8]{Mass_center_ux_displacements}
 		\caption{Pile mass center $u_{x}$ displacements.}
 		\label{MassCenterUxDisplacements}
 	\end{center}
 \end{figure}
   
 The initial inclination was set up for all three models (see Section \ref{subchap: Results}). The inclination after one and two hammer blows was calculated using the mean data of the four nodes shown in Figure \ref{PilePoints} at two different levels: at pile toe and at seabed level (13\,m altitude difference). The mean displacements in x and z direction were used to calculate the resulting inclination angle after each hammer blow. $\alpha_{f}$ as shown exaggerated in Figure \ref{InclinatioCalculationMethod} was calculated with the tangent of the values $u_{x}$ and $u_{z}$ using Equation \ref{TanAlpha}. The values obtained of $\alpha_{f}$ for the different pile and load configurations were compared with the fixed $\alpha_{i}$ values and the difference of these $\Delta\alpha$ was obtained after each of the two hammer blows. The angles deviation per blow for all three models are displayed on the graph in Figure \ref{ChangePileInclination}.  
 
 \begin{equation} 
 \label{TanAlpha}
 \alpha_{f}=\arctan({\frac{u_{x}}{u_{y}}}) 
 \end{equation}
 
 \begin{figure}[!]
 	\begin{center}
 		\includegraphics[scale=0.55]{Pile_inclination_calculation_method}
 		\caption{Pile final inclination calculation method.}
 		\label{InclinatioCalculationMethod}
 	\end{center}
 \end{figure}

\begin{figure}[!]
	\begin{center}
		\includegraphics[scale=0.8]{Change_pile_inclination}
		\caption{Change in the pile inclination $\Delta\alpha$ per blow for all models.}
		\label{ChangePileInclination}
	\end{center}
\end{figure}  
  
The tendency showed in Figure \ref{ChangePileInclination} is clear. For the eccentric load case the inclination tends to grow after each blow, while for the concentric case the inclination level remains almost the same and for the case with a pile inclination of 0,25\,$^\circ$ it is even reduced as it can be seen in Table \ref{tab:InclinationChange}. It is also to be noted, that for a higher initial tilt the percentage change of inclination with respect to the initial inclination is a bit lower, but the total angle change remains higher for upper initial angles. The fact that a lightly inclined MP continues to incline after a water hammer blow can be critical considering the strict tolerances that the industry has on pile inclination angles. This can be a big disadvantage of the water over the hydraulic hammer, due to the difficult of straighten the pile once it has reached a certain penetration depth. 

\begin{table}[htbp]
	\centering
	\caption{Inclination change per blow in [\%]}
	\begin{tabular}{|c|c|c|}
			\hline
		Model & One blow in \% & Two blows in \% \\ 	\hline
		Model 2 ecc & 16,68 & 20,6 \\ \hline
		Model 2 conc & -0,44 & -0,84 \\ \hline
		Model 3 ecc & 14,74 & 17,58 \\ \hline
		Model 3 conc & 0,54  & 0,36 \\ \hline
	\end{tabular}
	\label{tab:InclinationChange}
\end{table}

An unexpected result that can be seen in Figure \ref{ChangePileInclination} is, that the model with a straight pile inclines 0,0042\,$^\circ$ in the z-x plane after two concentric hammer blows. Figure \ref{ChangeStraightPileInclination} shows the inclination change of the straight pile model in both planes (z-x and z-y). The inclination change in plane z-y, is around four times lesser than the $\Delta\alpha$ in z-x plane nevertheless this is not an expected result. To look further into this issue, the displacements $u_{x}$ and $u_{y}$ in the planes A-A (x-z) and B-B (y-z) (see Figure \ref{ABPlanes}), which cut the pile in the middle, are displayed in Figures \ref{uxDisplacements} and \ref{uyDisplacements} respectively. This displacements should be completely symmetric for the case with a vertical pile, but this is not the case as can be seen in Figures \ref{uxDisplacements} and \ref{uyDisplacements}.   

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.55]{A-B_Planes}
		\caption{Planes A-A and B-B.}
		\label{ABPlanes}
	\end{center}
\end{figure}  

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.2]{Horizontal_Plane_ux}
		\caption{Displacements $u_{x}$ on plane A-A.}
		\label{uxDisplacements}
	\end{center}
\end{figure}

\begin{figure}[H]
	\begin{center}
		\includegraphics[scale=0.22]{Horizontal_Plane_uy}
		\caption{Displacements $u_{y}$ on plane B-B.}
		\label{uyDisplacements}
	\end{center}
\end{figure}

 One reason for the inclination change in the model with a straight pile can be a non-homogeneous and/or asymmetric void ratio distribution. Figures \ref{StraightPileVoidRatioInitialPhase} and \ref{StraightPileVoidRatio} show that the void ratio distribution at the pile activation phase and after one hammer blow in the z-x plane is not symmetric. The same happens with the effective stress $\sigma^{'}_{zz}$ distribution after one hammer blow showed in Figure \ref{StraightPileStresszz}. This issue originates by the activation type of the pile, the "wished in place" method. The used method for the pile activation has the weakness, that it does not takes into account the deformations and stresses that take place in the driving process until the initial penetration depth. Nevertheless this is an acceptable initial approach to the problem. This situation can be improved by further refining the mesh at the proximities of the pile.  

\begin{figure}[!]
	\begin{center}
		\includegraphics[scale=0.8]{Straight_pile_inclination_change}
		\caption{Straight pile model inclination change $\Delta\alpha$ per blow in z-x and z-y planes.}
		\label{ChangeStraightPileInclination}
	\end{center}
\end{figure}  

\begin{figure}[!]
	\begin{center}
		\includegraphics[scale=0.2]{Void_ratio_straight_pile}
		\caption{Straight pile model void ratio after the first blow.}
		\label{StraightPileVoidRatio}
	\end{center}
\end{figure} 

\begin{figure}[!]
	\begin{center}
		\includegraphics[scale=0.2]{Void_ratio_pile_activation}
		\caption{Straight pile model inclination change $\Delta\alpha$ per blow in z-x and z-y planes.}
		\label{StraightPileVoidRatioInitialPhase}
	\end{center}
\end{figure}   

\begin{figure}[!]
	\begin{center}
		\includegraphics[scale=0.2]{Effective_stress_zz_straight_pile}
		\caption{Effective stress $\sigma^{'}_{zz}$ after the first blow.}
		\label{StraightPileStresszz}
	\end{center}
\end{figure}  

Rhis chapter described the numerical simulation in detail. From input parameters to calculation phases until the results discussion was presented during Chapter \ref{chap: NumericalSimulation}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%