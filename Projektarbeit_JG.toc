\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {english}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline Abbildungsverzeichnis}{iii}{chapter*.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\nonumberline List of Tables}{v}{chapter*.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Theoretical framework}{3}{chapter.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Wind offshore industry}{3}{section.2.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Wind turbines foundation design and driving process}{4}{section.2.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Hammer evolution and types}{6}{section.2.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.1}Steam and compressed air hammer}{6}{subsection.2.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.2}Diesel hammer}{7}{subsection.2.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.3}Hydraulic hammer}{7}{subsection.2.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.4}Vibratory hammer}{7}{subsection.2.3.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3.5}Water hammer}{8}{subsection.2.3.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Monopile installation numerical simulation}{8}{section.2.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {3}Numerical simulation}{10}{chapter.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.1}Input parameters}{10}{section.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.1}Soil parameters}{11}{subsection.3.1.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.2}Pile simulation}{15}{subsection.3.1.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1.3}Load input}{16}{subsection.3.1.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.2}Mesh generation}{16}{section.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.3}Calculation phases}{17}{section.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.1}Initial phase}{17}{subsection.3.3.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.2}Pile activation phase}{18}{subsection.3.3.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3.3}Dynamic calculation phase}{18}{subsection.3.3.3}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.4}Plausibility check}{19}{section.3.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3.5}Simulation results}{22}{section.3.5}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.1}Driving efficiency}{23}{subsection.3.5.1}% 
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5.2}Pile inclination effects}{25}{subsection.3.5.2}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {4}Conclusions}{33}{chapter.4}% 
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {5}Bibliography}{34}{chapter.5}% 
